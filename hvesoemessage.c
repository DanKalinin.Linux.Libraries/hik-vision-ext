//
// Created by root on 07.09.2021.
//

#include "hvesoemessage.h"

gboolean hve_soe_message_set_error(HVESOEMessage *self, GError **error);

G_DEFINE_TYPE(HVESOEMessage, hve_soe_message, SOE_TYPE_MESSAGE)

static void hve_soe_message_class_init(HVESOEMessageClass *class) {

}

static void hve_soe_message_init(HVESOEMessage *self) {

}

gboolean hve_soe_message_set_error(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->response_body->length > 0) {
        gchar *content_type = (gchar *)soup_message_headers_get_content_type(SOUP_MESSAGE(self)->response_headers, NULL);

        if (g_strcmp0(content_type, XMLE_MIME_TYPE) == 0) {
            g_auto(xmlDocPtr) doc = NULL;
            if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return FALSE;
            xmlNodePtr node = xmlDocGetRootElement(doc);
            g_autoptr(HVEResponseStatus) response_status = hve_xmle_reader_response_status(node);
            hve_set_error(error, response_status);
        } else {
            soe_http_set_error(error, SOUP_MESSAGE(self)->status_code);
        }
    } else {
        soe_http_set_error(error, SOUP_MESSAGE(self)->status_code);
    }

    return TRUE;
}

HVESOEMessage *hve_soe_message_activate_status_get(SoupURI *base) {
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "SDK/activateStatus");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    return self;
}

HVEActivateStatus *hve_soe_message_activate_status_get_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEActivateStatus *ret = hve_xmle_reader_activate_status_get(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_challenge_get(SoupURI *base, HVEPublicKey *public_key) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_challenge_get(public_key);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "ISAPI/Security/challenge");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_POST, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEChallenge *hve_soe_message_challenge_get_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEChallenge *ret = hve_xmle_reader_challenge_get(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_activate(SoupURI *base, HVEActivateInfo *activate_info) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_activate(activate_info);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "ISAPI/System/activate");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEResponseStatus *hve_soe_message_activate_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_activate(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_reboot(SoupURI *base) {
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "ISAPI/System/reboot");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    return self;
}

HVEResponseStatus *hve_soe_message_reboot_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_reboot(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_device_info_get(SoupURI *base) {
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "ISAPI/System/deviceInfo");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    return self;
}

HVEDeviceInfo *hve_soe_message_device_info_get_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEDeviceInfo *ret = hve_xmle_reader_device_info_get(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_time_update(SoupURI *base, HVETime *time) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_time_update(time);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "ISAPI/System/time");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEResponseStatus *hve_soe_message_time_update_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_time_update(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_network_interfaces_get(SoupURI *base) {
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "ISAPI/System/Network/interfaces");

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    return self;
}

gboolean hve_soe_message_network_interfaces_get_finish(HVESOEMessage *self, GList **network_interfaces, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return FALSE;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        g_autolist(HVENetworkInterface) ret_network_interfaces = hve_xmle_reader_network_interfaces_get(node);
        GE_SET_VALUE(network_interfaces, g_steal_pointer(&ret_network_interfaces));
    } else {
        (void)hve_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

HVESOEMessage *hve_soe_message_wireless_get(SoupURI *base, gchar *id) {
    g_autofree gchar *tail = g_strdup_printf("ISAPI/System/Network/interfaces/%s/wireless", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    return self;
}

HVEWireless *hve_soe_message_wireless_get_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEWireless *ret = hve_xmle_reader_wireless_get(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_wireless_update(SoupURI *base, gchar *id, HVEWireless *wireless) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_wireless_update(wireless);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autofree gchar *tail = g_strdup_printf("ISAPI/System/Network/interfaces/%s/wireless", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEResponseStatus *hve_soe_message_wireless_update_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_wireless_update(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_wireless_update_v1(SoupURI *base, gchar *id, HVEWireless *wireless) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_wireless_update_v1(wireless);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autofree gchar *tail = g_strdup_printf("ISAPI/System/Network/interfaces/%s/wireless", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean hve_soe_message_wireless_update_v1_finish(HVESOEMessage *self, GError **error) {
    if ((SOUP_MESSAGE(self)->status_code == SOUP_STATUS_CANT_CONNECT) || (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_IO_ERROR)) {
        return TRUE;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return FALSE;
}

HVESOEMessage *hve_soe_message_ip_address_get(SoupURI *base, gchar *id) {
    g_autofree gchar *tail = g_strdup_printf("ISAPI/System/Network/interfaces/%s/ipAddress", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    return self;
}

HVEIPAddress *hve_soe_message_ip_address_get_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEIPAddress *ret = hve_xmle_reader_ip_address_get(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_ip_address_update(SoupURI *base, gchar *id, HVEIPAddress *ip_address) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_ip_address_update(ip_address);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autofree gchar *tail = g_strdup_printf("ISAPI/System/Network/interfaces/%s/ipAddress", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEResponseStatus *hve_soe_message_ip_address_update_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_ip_address_update(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_streaming_channel_update(SoupURI *base, gchar *id, HVEStreamingChannel *streaming_channel) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_streaming_channel_update(streaming_channel);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autofree gchar *tail = g_strdup_printf("ISAPI/Streaming/channels/%s", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEResponseStatus *hve_soe_message_streaming_channel_update_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_streaming_channel_update(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}

HVESOEMessage *hve_soe_message_motion_detection_update(SoupURI *base, gchar *id, HVEMotionDetection *motion_detection) {
    g_auto(xmlDocPtr) doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr node = hve_xmle_builder_motion_detection_update(motion_detection);
    (void)xmlDocSetRootElement(doc, node);

    gchar *body = NULL;
    gint length = 0;
    xmlDocDumpMemory(doc, (xmlChar **)&body, &length);

    g_autofree gchar *tail = g_strdup_printf("ISAPI/System/Video/inputs/channels/%s/MotionDetection", id);
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, tail);

    HVESOEMessage *self = g_object_new(HVE_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), XMLE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

HVEResponseStatus *hve_soe_message_motion_detection_update_finish(HVESOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_auto(xmlDocPtr) doc = NULL;
        if ((doc = xmleParseMemory((gchar *)SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) == NULL) return NULL;
        xmlNodePtr node = xmlDocGetRootElement(doc);
        HVEResponseStatus *ret = hve_xmle_reader_motion_detection_update(node);
        return ret;
    } else {
        (void)hve_soe_message_set_error(self, error);
    }

    return NULL;
}
