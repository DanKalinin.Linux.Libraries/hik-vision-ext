//
// Created by root on 30.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEAVHSERVICERESOLVER_H
#define LIBRARY_HIK_VISION_EXT_HVEAVHSERVICERESOLVER_H

#include "hvemain.h"

G_BEGIN_DECLS

#define HVE_TYPE_AVH_SERVICE_RESOLVER hve_avh_service_resolver_get_type()

GE_DECLARE_DERIVABLE_TYPE(HVEAVHServiceResolver, hve_avh_service_resolver, HVE, AVH_SERVICE_RESOLVER, AVHSServiceResolver)

struct _HVEAVHServiceResolverClass {
    AVHSServiceResolverClass super;
};

struct _HVEAVHServiceResolver {
    AVHSServiceResolver super;
};

HVEAVHServiceResolver *hve_avh_service_resolver_new(gchar *name, gchar *type, GError **error);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEAVHSERVICERESOLVER_H
