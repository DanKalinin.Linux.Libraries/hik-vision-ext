//
// Created by root on 07.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVESOESESSION_H
#define LIBRARY_HIK_VISION_EXT_HVESOESESSION_H

#include "hvemain.h"
#include "hvesoelogger.h"
#include "hvesoemessage.h"
#include "hveprivatekey.h"

G_BEGIN_DECLS

#define HVE_TYPE_SOE_SESSION hve_soe_session_get_type()

GE_DECLARE_DERIVABLE_TYPE(HVESOESession, hve_soe_session, HVE, SOE_SESSION, SOESession)

struct _HVESOESessionClass {
    SOESessionClass super;
};

struct _HVESOESession {
    SOESession super;

    SoupURI *base;
    gchar *username;
    gchar *password;
};

GE_STRUCTURE_FIELD(hve_soe_session, base, HVESOESession, SoupURI, soup_uri_copy, soup_uri_free, NULL)
GE_STRUCTURE_FIELD(hve_soe_session, username, HVESOESession, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_soe_session, password, HVESOESession, gchar, g_strdup, g_free, NULL)

HVESOESession *hve_soe_session_new(SoupURI *base, gchar *username, gchar *password);

HVEActivateStatus *hve_soe_session_activate_status_get_sync(HVESOESession *self, GError **error);
HVEChallenge *hve_soe_session_challenge_get_sync(HVESOESession *self, HVEPublicKey *public_key, GError **error);
HVEResponseStatus *hve_soe_session_activate_sync(HVESOESession *self, HVEActivateInfo *activate_info, GError **error);
HVEResponseStatus *hve_soe_session_reboot_sync(HVESOESession *self, GError **error);
HVEDeviceInfo *hve_soe_session_device_info_get_sync(HVESOESession *self, GError **error);
HVEResponseStatus *hve_soe_session_time_update_sync(HVESOESession *self, HVETime *time, GError **error);
gboolean hve_soe_session_network_interfaces_get_sync(HVESOESession *self, GList **network_interfaces, GError **error);
HVEWireless *hve_soe_session_wireless_get_sync(HVESOESession *self, gchar *id, GError **error);
HVEResponseStatus *hve_soe_session_wireless_update_sync(HVESOESession *self, gchar *id, HVEWireless *wireless, GError **error);
gboolean hve_soe_session_wireless_update_v1_sync(HVESOESession *self, gchar *id, HVEWireless *wireless, GError **error);
HVEIPAddress *hve_soe_session_ip_address_get_sync(HVESOESession *self, gchar *id, GError **error);
HVEResponseStatus *hve_soe_session_ip_address_update_sync(HVESOESession *self, gchar *id, HVEIPAddress *ip_address, GError **error);
HVEResponseStatus *hve_soe_session_streaming_channel_update_sync(HVESOESession *self, gchar *id, HVEStreamingChannel *streaming_channel, GError **error);
HVEResponseStatus *hve_soe_session_motion_detection_update_sync(HVESOESession *self, gchar *id, HVEMotionDetection *motion_detection, GError **error);

HVEResponseStatus *hve_soe_session_activate_v1_sync(HVESOESession *self, HVEActivateInfo *activate_info, GError **error);
HVEResponseStatus *hve_soe_session_time_update_v1_sync(HVESOESession *self, GError **error);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVESOESESSION_H
