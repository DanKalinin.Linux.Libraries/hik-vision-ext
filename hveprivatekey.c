//
// Created by root on 12.09.2021.
//

#include "hveprivatekey.h"

gchar hve_private_key_data[] =
        "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXQIBAAKBgQCVF6n0IdUFroWskKhjd/nXU6BQTBAwibSGeLcWv1ewM2frIokK\n"
        "v/zxNfFh+ON8Dimu6SAxDwfNXqRAaXXG0dXFGiUM8vDQtLhgWs3YeOTARHbyAc4R\n"
        "2uGMdp18AfRHDEIeHM5GadVsVIdoSneEdCjfk5miJp0nNRWfeRmlCG4CDQIDAQAB\n"
        "AoGATuY5AIMu8lTLVOkw04LYasVMn3HBV45oRIl5isYzp4Fsyxh5oYC8YQIE/a3y\n"
        "5OGx2WhxqsSjgmFFVvj4ZQECuP8wrTesV2L8Jspa7UdOYDw1Ir2H80rFEyAZeJ/t\n"
        "YLm8YTyvETC3CCqqxqKFdMHjZhtoBuT4ILd/OsT6dHIE0xkCQQDGq0KQ/QVOS9MP\n"
        "3Lb/pYI53b0ByjVvpB8WErODgpZ+uBFheHImcijz3DcCh5qEGayV9OlXRTzM23Ww\n"
        "iJsJobeXAkEAwB3rVJSgV57V2QFm5qBemoZRi9PYvNhvtzlrq6XAvUYPcLUjWT/q\n"
        "LwACZlzHgE34YH5WMA+jyP9CEQrZsDEn+wJBALkrjxPMgVGE4uh59Cc7gLGmtzAf\n"
        "5MZ11nOgYqlE4jiiH7EVrm+fIASLUMqO0bomo/HxIDwzUIQwWPV9nePQU2kCQAa1\n"
        "xM9qdYM57/wpWV+6wDMjd9urZYmLAbPjlIglPOsfdFCacwad/d7wYCUVHn6zunYc\n"
        "U4RQN4dAqx0PRPUrclsCQQC/FpIkBPA88KSHktEkxX47j4kAy+H58Z2nRInQckac\n"
        "pT2WL0Lr/fMirjnNgzAuvs4Q88IR6xx4UFDHcigvbuar\n"
        "-----END RSA PRIVATE KEY-----";

gnutls_datum_t hve_private_key = {(guchar *)hve_private_key_data, sizeof(hve_private_key_data)};
