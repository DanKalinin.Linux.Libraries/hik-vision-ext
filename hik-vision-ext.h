//
// Created by root on 06.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HIK_VISION_EXT_H
#define LIBRARY_HIK_VISION_EXT_HIK_VISION_EXT_H

#include <hik-vision-ext/hvemain.h>
#include <hik-vision-ext/hveerror.h>
#include <hik-vision-ext/hveschema.h>
#include <hik-vision-ext/hvesoelogger.h>
#include <hik-vision-ext/hvesoemessage.h>
#include <hik-vision-ext/hvesoesession.h>
#include <hik-vision-ext/hvexmlebuilder.h>
#include <hik-vision-ext/hvexmlereader.h>
#include <hik-vision-ext/hveprivatekey.h>
#include <hik-vision-ext/hveavhservicebrowser.h>
#include <hik-vision-ext/hveavhserviceresolver.h>
#include <hik-vision-ext/hveinit.h>

#endif //LIBRARY_HIK_VISION_EXT_HIK_VISION_EXT_H
