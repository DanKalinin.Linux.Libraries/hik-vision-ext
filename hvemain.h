//
// Created by root on 06.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEMAIN_H
#define LIBRARY_HIK_VISION_EXT_HVEMAIN_H

#include <glib-ext/glib-ext.h>
#include <soup-ext/soup-ext.h>
#include <gnutls-ext/gnutls-ext.h>
#include <glib-networking-ext/glib-networking-ext.h>
#include <xml-ext/xml-ext.h>
#include <avahi-ext/avahi-ext.h>

#endif //LIBRARY_HIK_VISION_EXT_HVEMAIN_H
