//
// Created by root on 09.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEXMLEBUILDER_H
#define LIBRARY_HIK_VISION_EXT_HVEXMLEBUILDER_H

#include "hvemain.h"
#include "hveschema.h"

G_BEGIN_DECLS

xmlNodePtr hve_xmle_builder_challenge_get(HVEPublicKey *public_key);
xmlNodePtr hve_xmle_builder_activate(HVEActivateInfo *activate_info);
xmlNodePtr hve_xmle_builder_time_update(HVETime *time);
xmlNodePtr hve_xmle_builder_wireless_update(HVEWireless *wireless);
xmlNodePtr hve_xmle_builder_wireless_update_v1(HVEWireless *wireless);
xmlNodePtr hve_xmle_builder_ip_address_update(HVEIPAddress *ip_address);
xmlNodePtr hve_xmle_builder_streaming_channel_update(HVEStreamingChannel *streaming_channel);
xmlNodePtr hve_xmle_builder_motion_detection_update(HVEMotionDetection *motion_detection);

xmlNodePtr hve_xmle_builder_public_key(HVEPublicKey *public_key);

xmlNodePtr hve_xmle_builder_activate_info(HVEActivateInfo *activate_info);

xmlNodePtr hve_xmle_builder_time(HVETime *time);

xmlNodePtr hve_xmle_builder_wpa(HVEWPA *wpa);

xmlNodePtr hve_xmle_builder_wireless_security(HVEWirelessSecurity *wireless_security);

xmlNodePtr hve_xmle_builder_wireless(HVEWireless *wireless);

xmlNodePtr hve_xmle_builder_ipv6_mode(HVEIPv6Mode *ipv6_mode);

xmlNodePtr hve_xmle_builder_ip_address(HVEIPAddress *ip_address);

xmlNodePtr hve_xmle_builder_video(HVEVideo *video);

xmlNodePtr hve_xmle_builder_audio(HVEAudio *audio);

xmlNodePtr hve_xmle_builder_streaming_channel(HVEStreamingChannel *streaming_channel);

xmlNodePtr hve_xmle_builder_grid(HVEGrid *grid);

xmlNodePtr hve_xmle_builder_layout(HVELayout *layout);

xmlNodePtr hve_xmle_builder_motion_detection_layout(HVEMotionDetectionLayout *motion_detection_layout);

xmlNodePtr hve_xmle_builder_motion_detection(HVEMotionDetection *motion_detection);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEXMLEBUILDER_H
