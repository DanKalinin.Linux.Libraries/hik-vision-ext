//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEINIT_H
#define LIBRARY_HIK_VISION_EXT_HVEINIT_H

#include "hvemain.h"

G_BEGIN_DECLS

void hve_init(void);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEINIT_H
