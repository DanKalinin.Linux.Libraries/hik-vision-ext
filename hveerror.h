//
// Created by root on 10.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEERROR_H
#define LIBRARY_HIK_VISION_EXT_HVEERROR_H

#include "hvemain.h"
#include "hveschema.h"

G_BEGIN_DECLS

#define HVE_ERROR hve_error_quark()

typedef enum {
    HVE_ERROR_FAILED
} HVEError;

GQuark hve_error_quark(void);

void hve_set_error(GError **self, HVEResponseStatus *response_status);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEERROR_H
