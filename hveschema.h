//
// Created by root on 07.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVESCHEMA_H
#define LIBRARY_HIK_VISION_EXT_HVESCHEMA_H

#include "hvemain.h"










G_BEGIN_DECLS

typedef struct {
    gchar *request_url;
    gint status_code;
    gchar *status_string;
    gchar *sub_status_code;
} HVEResponseStatus;

GE_STRUCTURE_FIELD(hve_response_status, request_url, HVEResponseStatus, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_response_status, status_string, HVEResponseStatus, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_response_status, sub_status_code, HVEResponseStatus, gchar, g_strdup, g_free, NULL)

HVEResponseStatus *hve_response_status_copy(HVEResponseStatus *self);
void hve_response_status_free(HVEResponseStatus *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEResponseStatus, hve_response_status_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gboolean activated;
} HVEActivateStatus;

HVEActivateStatus *hve_activate_status_copy(HVEActivateStatus *self);
void hve_activate_status_free(HVEActivateStatus *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEActivateStatus, hve_activate_status_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *key;
} HVEPublicKey;

GE_STRUCTURE_FIELD(hve_public_key, key, HVEPublicKey, gchar, g_strdup, g_free, NULL)

HVEPublicKey *hve_public_key_copy(HVEPublicKey *self);
void hve_public_key_free(HVEPublicKey *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEPublicKey, hve_public_key_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *key;
} HVEChallenge;

GE_STRUCTURE_FIELD(hve_challenge, key, HVEChallenge, gchar, g_strdup, g_free, NULL)

HVEChallenge *hve_challenge_copy(HVEChallenge *self);
void hve_challenge_free(HVEChallenge *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEChallenge, hve_challenge_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *password;
} HVEActivateInfo;

GE_STRUCTURE_FIELD(hve_activate_info, password, HVEActivateInfo, gchar, g_strdup, g_free, NULL)

HVEActivateInfo *hve_activate_info_copy(HVEActivateInfo *self);
void hve_activate_info_free(HVEActivateInfo *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEActivateInfo, hve_activate_info_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *device_name;
    gchar *device_id;
    gchar *device_description;
    gchar *device_location;
    gchar *system_contact;
    gchar *model;
    gchar *serial_number;
    gchar *mac_address;
    gchar *firmware_version;
    gchar *firmware_released_date;
    gchar *boot_version;
    gchar *boot_released_date;
    gchar *hardware_version;
    gchar *encoder_version;
    gchar *encoder_released_date;
    gchar *decoder_version;
    gchar *decoder_released_date;
    gchar *device_type;
    gint telecontrol_id;
    gboolean support_beep;
    gchar *firmware_version_info;
} HVEDeviceInfo;

GE_STRUCTURE_FIELD(hve_device_info, device_name, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, device_id, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, device_description, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, device_location, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, system_contact, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, model, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, serial_number, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, mac_address, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, firmware_version, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, firmware_released_date, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, boot_version, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, boot_released_date, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, hardware_version, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, encoder_version, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, encoder_released_date, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, decoder_version, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, decoder_released_date, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, device_type, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_device_info, firmware_version_info, HVEDeviceInfo, gchar, g_strdup, g_free, NULL)

HVEDeviceInfo *hve_device_info_copy(HVEDeviceInfo *self);
void hve_device_info_free(HVEDeviceInfo *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEDeviceInfo, hve_device_info_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *time_mode;
    gchar *local_time;
    gchar *time_zone;
} HVETime;

GE_STRUCTURE_FIELD(hve_time, time_mode, HVETime, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_time, local_time, HVETime, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_time, time_zone, HVETime, gchar, g_strdup, g_free, NULL)

HVETime *hve_time_copy(HVETime *self);
void hve_time_free(HVETime *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVETime, hve_time_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *id;
    gboolean default_connection;
    gchar *mac_address;
} HVENetworkInterface;

GE_STRUCTURE_FIELD(hve_network_interface, id, HVENetworkInterface, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_network_interface, mac_address, HVENetworkInterface, gchar, g_strdup, g_free, NULL)

HVENetworkInterface *hve_network_interface_copy(HVENetworkInterface *self);
void hve_network_interface_free(HVENetworkInterface *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVENetworkInterface, hve_network_interface_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *algorithm_type;
    gchar *shared_key;
    gint wpa_key_length;
} HVEWPA;

GE_STRUCTURE_FIELD(hve_wpa, algorithm_type, HVEWPA, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_wpa, shared_key, HVEWPA, gchar, g_strdup, g_free, NULL)

HVEWPA *hve_wpa_copy(HVEWPA *self);
void hve_wpa_free(HVEWPA *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEWPA, hve_wpa_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *security_mode;
    HVEWPA *wpa;
} HVEWirelessSecurity;

GE_STRUCTURE_FIELD(hve_wireless_security, security_mode, HVEWirelessSecurity, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_wireless_security, wpa, HVEWirelessSecurity, HVEWPA, hve_wpa_copy, hve_wpa_free, NULL)

HVEWirelessSecurity *hve_wireless_security_copy(HVEWirelessSecurity *self);
void hve_wireless_security_free(HVEWirelessSecurity *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEWirelessSecurity, hve_wireless_security_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gboolean enabled;
    gchar *ssid;
    HVEWirelessSecurity *wireless_security;
} HVEWireless;

GE_STRUCTURE_FIELD(hve_wireless, ssid, HVEWireless, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_wireless, wireless_security, HVEWireless, HVEWirelessSecurity, hve_wireless_security_copy, hve_wireless_security_free, NULL)

HVEWireless *hve_wireless_copy(HVEWireless *self);
void hve_wireless_free(HVEWireless *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEWireless, hve_wireless_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *ipv6_addressing_type;
} HVEIPv6Mode;

GE_STRUCTURE_FIELD(hve_ipv6_mode, ipv6_addressing_type, HVEIPv6Mode, gchar, g_strdup, g_free, NULL)

HVEIPv6Mode *hve_ipv6_mode_copy(HVEIPv6Mode *self);
void hve_ipv6_mode_free(HVEIPv6Mode *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEIPv6Mode, hve_ipv6_mode_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *ip_version;
    gchar *addressing_type;
    HVEIPv6Mode *ipv6_mode;
} HVEIPAddress;

GE_STRUCTURE_FIELD(hve_ip_address, ip_version, HVEIPAddress, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_ip_address, addressing_type, HVEIPAddress, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_ip_address, ipv6_mode, HVEIPAddress, HVEIPv6Mode, hve_ipv6_mode_copy, hve_ipv6_mode_free, NULL)

HVEIPAddress *hve_ip_address_copy(HVEIPAddress *self);
void hve_ip_address_free(HVEIPAddress *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEIPAddress, hve_ip_address_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *video_codec_type;
    gint video_resolution_width;
    gint video_resolution_height;
    gchar *video_quality_control_type;
    gint max_frame_rate;
} HVEVideo;

GE_STRUCTURE_FIELD(hve_video, video_codec_type, HVEVideo, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_video, video_quality_control_type, HVEVideo, gchar, g_strdup, g_free, NULL)

HVEVideo *hve_video_copy(HVEVideo *self);
void hve_video_free(HVEVideo *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEVideo, hve_video_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gboolean enabled;
    gchar *audio_compression_type;
} HVEAudio;

GE_STRUCTURE_FIELD(hve_audio, audio_compression_type, HVEAudio, gchar, g_strdup, g_free, NULL)

HVEAudio *hve_audio_copy(HVEAudio *self);
void hve_audio_free(HVEAudio *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEAudio, hve_audio_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    HVEVideo *video;
    HVEAudio *audio;
} HVEStreamingChannel;

GE_STRUCTURE_FIELD(hve_streaming_channel, video, HVEStreamingChannel, HVEVideo, hve_video_copy, hve_video_free, NULL)
GE_STRUCTURE_FIELD(hve_streaming_channel, audio, HVEStreamingChannel, HVEAudio, hve_audio_copy, hve_audio_free, NULL)

HVEStreamingChannel *hve_streaming_channel_copy(HVEStreamingChannel *self);
void hve_streaming_channel_free(HVEStreamingChannel *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEStreamingChannel, hve_streaming_channel_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gint row_granularity;
    gint column_granularity;
} HVEGrid;

HVEGrid *hve_grid_copy(HVEGrid *self);
void hve_grid_free(HVEGrid *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEGrid, hve_grid_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *grid_map;
} HVELayout;

GE_STRUCTURE_FIELD(hve_layout, grid_map, HVELayout, gchar, g_strdup, g_free, NULL)

HVELayout *hve_layout_copy(HVELayout *self);
void hve_layout_free(HVELayout *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVELayout, hve_layout_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gint sensitivity_level;
    HVELayout *layout;
} HVEMotionDetectionLayout;

GE_STRUCTURE_FIELD(hve_motion_detection_layout, layout, HVEMotionDetectionLayout, HVELayout, hve_layout_copy, hve_layout_free, NULL)

HVEMotionDetectionLayout *hve_motion_detection_layout_copy(HVEMotionDetectionLayout *self);
void hve_motion_detection_layout_free(HVEMotionDetectionLayout *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEMotionDetectionLayout, hve_motion_detection_layout_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gboolean enabled;
    HVEGrid *grid;
    HVEMotionDetectionLayout *motion_detection_layout;
} HVEMotionDetection;

GE_STRUCTURE_FIELD(hve_motion_detection, grid, HVEMotionDetection, HVEGrid, hve_grid_copy, hve_grid_free, NULL)
GE_STRUCTURE_FIELD(hve_motion_detection, motion_detection_layout, HVEMotionDetection, HVEMotionDetectionLayout, hve_motion_detection_layout_copy, hve_motion_detection_layout_free, NULL)

HVEMotionDetection *hve_motion_detection_copy(HVEMotionDetection *self);
void hve_motion_detection_free(HVEMotionDetection *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEMotionDetection, hve_motion_detection_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *name;
    gchar *host;
    gchar *ip;
    gint port;
    gchar *path;
} HVEEndpoint;

GE_STRUCTURE_FIELD(hve_endpoint, name, HVEEndpoint, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_endpoint, host, HVEEndpoint, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_endpoint, ip, HVEEndpoint, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(hve_endpoint, path, HVEEndpoint, gchar, g_strdup, g_free, NULL)

HVEEndpoint *hve_endpoint_copy(HVEEndpoint *self);
void hve_endpoint_free(HVEEndpoint *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(HVEEndpoint, hve_endpoint_free)

G_END_DECLS










#endif //LIBRARY_HIK_VISION_EXT_HVESCHEMA_H
