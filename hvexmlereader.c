//
// Created by root on 09.09.2021.
//

#include "hvexmlereader.h"

HVEActivateStatus *hve_xmle_reader_activate_status_get(xmlNodePtr self) {
    HVEActivateStatus *ret = hve_xmle_reader_activate_status(self);
    return ret;
}

HVEChallenge *hve_xmle_reader_challenge_get(xmlNodePtr self) {
    HVEChallenge *ret = hve_xmle_reader_challenge(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_activate(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_reboot(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

HVEDeviceInfo *hve_xmle_reader_device_info_get(xmlNodePtr self) {
    HVEDeviceInfo *ret = hve_xmle_reader_device_info(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_time_update(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

GList *hve_xmle_reader_network_interfaces_get(xmlNodePtr self) {
    GList *ret = hve_xmle_reader_network_interfaces(self);
    return ret;
}

HVEWireless *hve_xmle_reader_wireless_get(xmlNodePtr self) {
    HVEWireless *ret = hve_xmle_reader_wireless(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_wireless_update(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

HVEIPAddress *hve_xmle_reader_ip_address_get(xmlNodePtr self) {
    HVEIPAddress *ret = hve_xmle_reader_ip_address(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_ip_address_update(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_streaming_channel_update(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

HVEResponseStatus *hve_xmle_reader_motion_detection_update(xmlNodePtr self) {
    HVEResponseStatus *ret = hve_xmle_reader_response_status(self);
    return ret;
}

HVEActivateStatus *hve_xmle_reader_activate_status(xmlNodePtr self) {
    HVEActivateStatus *ret = g_new0(HVEActivateStatus, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "Activated")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->activated = g_str_equal(content, "true");
        }
    }

    return ret;
}

HVEChallenge *hve_xmle_reader_challenge(xmlNodePtr self) {
    HVEChallenge *ret = g_new0(HVEChallenge, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "key")) {
            ret->key = (gchar *)xmlNodeGetContent(node);
        }
    }

    return ret;
}

HVEDeviceInfo *hve_xmle_reader_device_info(xmlNodePtr self) {
    HVEDeviceInfo *ret = g_new0(HVEDeviceInfo, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "deviceName")) {
            ret->device_name = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "deviceID")) {
            ret->device_id = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "deviceDescription")) {
            ret->device_description = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "deviceLocation")) {
            ret->device_location = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "systemContact")) {
            ret->system_contact = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "model")) {
            ret->model = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "serialNumber")) {
            ret->serial_number = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "macAddress")) {
            ret->mac_address = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "firmwareVersion")) {
            ret->firmware_version = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "firmwareReleasedDate")) {
            ret->firmware_released_date = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "bootVersion")) {
            ret->boot_version = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "bootReleasedDate")) {
            ret->boot_released_date = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "hardwareVersion")) {
            ret->hardware_version = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "encoderVersion")) {
            ret->encoder_version = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "encoderReleasedDate")) {
            ret->encoder_released_date = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "decoderVersion")) {
            ret->decoder_version = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "decoderReleasedDate")) {
            ret->decoder_released_date = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "deviceType")) {
            ret->device_type = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "telecontrolID")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->telecontrol_id = (gint)strtol(content, NULL, 10);
        } else if (g_str_equal(node->name, "supportBeep")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->support_beep = g_str_equal(content, "true");
        } else if (g_str_equal(node->name, "firmwareVersionInfo")) {
            ret->firmware_version_info = (gchar *)xmlNodeGetContent(node);
        }
    }

    return ret;
}

HVENetworkInterface *hve_xmle_reader_network_interface(xmlNodePtr self) {
    HVENetworkInterface *ret = g_new0(HVENetworkInterface, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "id")) {
            ret->id = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "defaultConnection")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->default_connection = g_str_equal(content, "true");
        } else if (g_str_equal(node->name, "macAddress")) {
            ret->mac_address = (gchar *)xmlNodeGetContent(node);
        }
    }

    return ret;
}

GList *hve_xmle_reader_network_interfaces(xmlNodePtr self) {
    GList *ret = NULL;

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "NetworkInterface")) {
            HVENetworkInterface *network_interface = hve_xmle_reader_network_interface(node);
            ret = g_list_append(ret, network_interface);
        }
    }

    return ret;
}

HVEResponseStatus *hve_xmle_reader_response_status(xmlNodePtr self) {
    HVEResponseStatus *ret = g_new0(HVEResponseStatus, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "requestURL")) {
            ret->request_url = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "statusCode")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->status_code = (gint)strtol(content, NULL, 10);
        } else if (g_str_equal(node->name, "statusString")) {
            ret->status_string = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "subStatusCode")) {
            ret->sub_status_code = (gchar *)xmlNodeGetContent(node);
        }
    }

    return ret;
}
HVEWPA *hve_xmle_reader_wpa(xmlNodePtr self) {
    HVEWPA *ret = g_new0(HVEWPA, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "algorithmType")) {
            ret->algorithm_type = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "sharedKey")) {
            ret->shared_key = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "wpaKeyLength")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->wpa_key_length = (gint)strtol(content, NULL, 10);
        }
    }

    return ret;
}

HVEWirelessSecurity *hve_xmle_reader_wireless_security(xmlNodePtr self) {
    HVEWirelessSecurity *ret = g_new0(HVEWirelessSecurity, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "securityMode")) {
            ret->security_mode = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "WPA")) {
            ret->wpa = hve_xmle_reader_wpa(node);
        }
    }

    return ret;
}

HVEWireless *hve_xmle_reader_wireless(xmlNodePtr self) {
    HVEWireless *ret = g_new0(HVEWireless, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "enabled")) {
            g_autofree gchar *content = (gchar *)xmlNodeGetContent(node);
            ret->enabled = g_str_equal(content, "true");
        } else if (g_str_equal(node->name, "ssid")) {
            ret->ssid = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "WirelessSecurity")) {
            ret->wireless_security = hve_xmle_reader_wireless_security(node);
        }
    }

    return ret;
}

HVEIPv6Mode *hve_xmle_reader_ipv6_mode(xmlNodePtr self) {
    HVEIPv6Mode *ret = g_new0(HVEIPv6Mode, 1);
    
    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "ipV6AddressingType")) {
            ret->ipv6_addressing_type = (gchar *)xmlNodeGetContent(node);
        }
    }
    
    return ret;
}

HVEIPAddress *hve_xmle_reader_ip_address(xmlNodePtr self) {
    HVEIPAddress *ret = g_new0(HVEIPAddress, 1);

    for (xmlNodePtr node = self->xmlChildrenNode; node != NULL; node = node->next) {
        if (g_str_equal(node->name, "ipVersion")) {
            ret->ip_version = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "addressingType")) {
            ret->addressing_type = (gchar *)xmlNodeGetContent(node);
        } else if (g_str_equal(node->name, "Ipv6Mode")) {
            ret->ipv6_mode = hve_xmle_reader_ipv6_mode(node);
        }
    }

    return ret;
}
