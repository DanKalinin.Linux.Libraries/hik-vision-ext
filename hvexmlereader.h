//
// Created by root on 09.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEXMLEREADER_H
#define LIBRARY_HIK_VISION_EXT_HVEXMLEREADER_H

#include "hvemain.h"
#include "hveschema.h"

G_BEGIN_DECLS

HVEActivateStatus *hve_xmle_reader_activate_status_get(xmlNodePtr self);
HVEChallenge *hve_xmle_reader_challenge_get(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_activate(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_reboot(xmlNodePtr self);
HVEDeviceInfo *hve_xmle_reader_device_info_get(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_time_update(xmlNodePtr self);
GList *hve_xmle_reader_network_interfaces_get(xmlNodePtr self);
HVEWireless *hve_xmle_reader_wireless_get(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_wireless_update(xmlNodePtr self);
HVEIPAddress *hve_xmle_reader_ip_address_get(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_ip_address_update(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_streaming_channel_update(xmlNodePtr self);
HVEResponseStatus *hve_xmle_reader_motion_detection_update(xmlNodePtr self);

HVEActivateStatus *hve_xmle_reader_activate_status(xmlNodePtr self);

HVEChallenge *hve_xmle_reader_challenge(xmlNodePtr self);

HVEDeviceInfo *hve_xmle_reader_device_info(xmlNodePtr self);

HVENetworkInterface *hve_xmle_reader_network_interface(xmlNodePtr self);
GList *hve_xmle_reader_network_interfaces(xmlNodePtr self);

HVEResponseStatus *hve_xmle_reader_response_status(xmlNodePtr self);

HVEWPA *hve_xmle_reader_wpa(xmlNodePtr self);

HVEWirelessSecurity *hve_xmle_reader_wireless_security(xmlNodePtr self);

HVEWireless *hve_xmle_reader_wireless(xmlNodePtr self);

HVEIPv6Mode *hve_xmle_reader_ipv6_mode(xmlNodePtr self);

HVEIPAddress *hve_xmle_reader_ip_address(xmlNodePtr self);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEXMLEREADER_H
