//
// Created by root on 07.09.2021.
//

#include "hveschema.h"










HVEResponseStatus *hve_response_status_copy(HVEResponseStatus *self) {
    HVEResponseStatus *ret = g_new0(HVEResponseStatus, 1);
    hve_response_status_set_request_url(ret, self->request_url);
    ret->status_code = self->status_code;
    hve_response_status_set_status_string(ret, self->status_string);
    hve_response_status_set_sub_status_code(ret, self->sub_status_code);
    return ret;
}

void hve_response_status_free(HVEResponseStatus *self) {
    hve_response_status_set_request_url(self, NULL);
    hve_response_status_set_status_string(self, NULL);
    hve_response_status_set_sub_status_code(self, NULL);
    g_free(self);
}










HVEActivateStatus *hve_activate_status_copy(HVEActivateStatus *self) {
    HVEActivateStatus *ret = g_new0(HVEActivateStatus, 1);
    ret->activated = self->activated;
    return ret;
}

void hve_activate_status_free(HVEActivateStatus *self) {
    g_free(self);
}










HVEPublicKey *hve_public_key_copy(HVEPublicKey *self) {
    HVEPublicKey *ret = g_new0(HVEPublicKey, 1);
    hve_public_key_set_key(ret, self->key);
    return ret;
}

void hve_public_key_free(HVEPublicKey *self) {
    hve_public_key_set_key(self, NULL);
    g_free(self);
}










HVEChallenge *hve_challenge_copy(HVEChallenge *self) {
    HVEChallenge *ret = g_new0(HVEChallenge, 1);
    hve_challenge_set_key(ret, self->key);
    return ret;
}

void hve_challenge_free(HVEChallenge *self) {
    hve_challenge_set_key(self, NULL);
    g_free(self);
}










HVEActivateInfo *hve_activate_info_copy(HVEActivateInfo *self) {
    HVEActivateInfo *ret = g_new0(HVEActivateInfo, 1);
    hve_activate_info_set_password(ret, self->password);
    return ret;
}

void hve_activate_info_free(HVEActivateInfo *self) {
    hve_activate_info_set_password(self, NULL);
    g_free(self);
}










HVEDeviceInfo *hve_device_info_copy(HVEDeviceInfo *self) {
    HVEDeviceInfo *ret = g_new0(HVEDeviceInfo, 1);
    hve_device_info_set_device_name(ret, self->device_name);
    hve_device_info_set_device_id(ret, self->device_id);
    hve_device_info_set_device_description(ret, self->device_description);
    hve_device_info_set_device_location(ret, self->device_location);
    hve_device_info_set_system_contact(ret, self->system_contact);
    hve_device_info_set_model(ret, self->model);
    hve_device_info_set_serial_number(ret, self->serial_number);
    hve_device_info_set_mac_address(ret, self->mac_address);
    hve_device_info_set_firmware_version(ret, self->firmware_version);
    hve_device_info_set_firmware_released_date(ret, self->firmware_released_date);
    hve_device_info_set_boot_version(ret, self->boot_version);
    hve_device_info_set_boot_released_date(ret, self->boot_released_date);
    hve_device_info_set_hardware_version(ret, self->hardware_version);
    hve_device_info_set_encoder_version(ret, self->encoder_version);
    hve_device_info_set_encoder_released_date(ret, self->encoder_released_date);
    hve_device_info_set_decoder_version(ret, self->decoder_version);
    hve_device_info_set_decoder_released_date(ret, self->decoder_released_date);
    hve_device_info_set_device_type(ret, self->device_type);
    ret->telecontrol_id = self->telecontrol_id;
    ret->support_beep = self->support_beep;
    hve_device_info_set_firmware_version_info(ret, self->firmware_version_info);
    return ret;
}

void hve_device_info_free(HVEDeviceInfo *self) {
    hve_device_info_set_device_name(self, NULL);
    hve_device_info_set_device_id(self, NULL);
    hve_device_info_set_device_description(self, NULL);
    hve_device_info_set_device_location(self, NULL);
    hve_device_info_set_system_contact(self, NULL);
    hve_device_info_set_model(self, NULL);
    hve_device_info_set_serial_number(self, NULL);
    hve_device_info_set_mac_address(self, NULL);
    hve_device_info_set_firmware_version(self, NULL);
    hve_device_info_set_firmware_released_date(self, NULL);
    hve_device_info_set_boot_version(self, NULL);
    hve_device_info_set_boot_released_date(self, NULL);
    hve_device_info_set_hardware_version(self, NULL);
    hve_device_info_set_encoder_version(self, NULL);
    hve_device_info_set_encoder_released_date(self, NULL);
    hve_device_info_set_decoder_version(self, NULL);
    hve_device_info_set_decoder_released_date(self, NULL);
    hve_device_info_set_device_type(self, NULL);
    hve_device_info_set_firmware_version_info(self, NULL);
    g_free(self);
}










HVETime *hve_time_copy(HVETime *self) {
    HVETime *ret = g_new0(HVETime, 1);
    hve_time_set_time_mode(ret, self->time_mode);
    hve_time_set_local_time(ret, self->local_time);
    hve_time_set_time_zone(ret, self->time_zone);
    return ret;
}

void hve_time_free(HVETime *self) {
    hve_time_set_time_mode(self, NULL);
    hve_time_set_local_time(self, NULL);
    hve_time_set_time_zone(self, NULL);
    g_free(self);
}










HVENetworkInterface *hve_network_interface_copy(HVENetworkInterface *self) {
    HVENetworkInterface *ret = g_new0(HVENetworkInterface, 1);
    hve_network_interface_set_id(ret, self->id);
    ret->default_connection = self->default_connection;
    hve_network_interface_set_mac_address(ret, self->mac_address);
    return ret;
}

void hve_network_interface_free(HVENetworkInterface *self) {
    hve_network_interface_set_id(self, NULL);
    hve_network_interface_set_mac_address(self, NULL);
    g_free(self);
}










HVEWPA *hve_wpa_copy(HVEWPA *self) {
    HVEWPA *ret = g_new0(HVEWPA, 1);
    hve_wpa_set_algorithm_type(ret, self->algorithm_type);
    hve_wpa_set_shared_key(ret, self->shared_key);
    ret->wpa_key_length = self->wpa_key_length;
    return ret;
}

void hve_wpa_free(HVEWPA *self) {
    hve_wpa_set_algorithm_type(self, NULL);
    hve_wpa_set_shared_key(self, NULL);
    g_free(self);
}










HVEWirelessSecurity *hve_wireless_security_copy(HVEWirelessSecurity *self) {
    HVEWirelessSecurity *ret = g_new0(HVEWirelessSecurity, 1);
    hve_wireless_security_set_security_mode(ret, self->security_mode);
    hve_wireless_security_set_wpa(ret, self->wpa);
    return ret;
}

void hve_wireless_security_free(HVEWirelessSecurity *self) {
    hve_wireless_security_set_security_mode(self, NULL);
    hve_wireless_security_set_wpa(self, NULL);
    g_free(self);
}










HVEWireless *hve_wireless_copy(HVEWireless *self) {
    HVEWireless *ret = g_new0(HVEWireless, 1);
    ret->enabled = self->enabled;
    hve_wireless_set_ssid(ret, self->ssid);
    hve_wireless_set_wireless_security(ret, self->wireless_security);
    return ret;
}

void hve_wireless_free(HVEWireless *self) {
    hve_wireless_set_ssid(self, NULL);
    hve_wireless_set_wireless_security(self, NULL);
    g_free(self);
}










HVEIPv6Mode *hve_ipv6_mode_copy(HVEIPv6Mode *self) {
    HVEIPv6Mode *ret = g_new0(HVEIPv6Mode, 1);
    hve_ipv6_mode_set_ipv6_addressing_type(ret, self->ipv6_addressing_type);
    return ret;
}

void hve_ipv6_mode_free(HVEIPv6Mode *self) {
    hve_ipv6_mode_set_ipv6_addressing_type(self, NULL);
    g_free(self);
}










HVEIPAddress *hve_ip_address_copy(HVEIPAddress *self) {
    HVEIPAddress *ret = g_new0(HVEIPAddress, 1);
    hve_ip_address_set_ip_version(ret, self->ip_version);
    hve_ip_address_set_addressing_type(ret, self->addressing_type);
    hve_ip_address_set_ipv6_mode(ret, self->ipv6_mode);
    return ret;
}

void hve_ip_address_free(HVEIPAddress *self) {
    hve_ip_address_set_ip_version(self, NULL);
    hve_ip_address_set_addressing_type(self, NULL);
    hve_ip_address_set_ipv6_mode(self, NULL);
    g_free(self);
}










HVEVideo *hve_video_copy(HVEVideo *self) {
    HVEVideo *ret = g_new0(HVEVideo, 1);
    hve_video_set_video_codec_type(ret, self->video_codec_type);
    ret->video_resolution_width = self->video_resolution_width;
    ret->video_resolution_height = self->video_resolution_height;
    hve_video_set_video_quality_control_type(ret, self->video_quality_control_type);
    ret->max_frame_rate = self->max_frame_rate;
    return ret;
}

void hve_video_free(HVEVideo *self) {
    hve_video_set_video_codec_type(self, NULL);
    hve_video_set_video_quality_control_type(self, NULL);
    g_free(self);
}










HVEAudio *hve_audio_copy(HVEAudio *self) {
    HVEAudio *ret = g_new0(HVEAudio, 1);
    ret->enabled = self->enabled;
    hve_audio_set_audio_compression_type(ret, self->audio_compression_type);
    return ret;
}

void hve_audio_free(HVEAudio *self) {
    hve_audio_set_audio_compression_type(self, NULL);
    g_free(self);
}










HVEStreamingChannel *hve_streaming_channel_copy(HVEStreamingChannel *self) {
    HVEStreamingChannel *ret = g_new0(HVEStreamingChannel, 1);
    hve_streaming_channel_set_video(ret, self->video);
    hve_streaming_channel_set_audio(ret, self->audio);
    return ret;
}

void hve_streaming_channel_free(HVEStreamingChannel *self) {
    hve_streaming_channel_set_video(self, NULL);
    hve_streaming_channel_set_audio(self, NULL);
    g_free(self);
}










HVEGrid *hve_grid_copy(HVEGrid *self) {
    HVEGrid *ret = g_new0(HVEGrid, 1);
    ret->row_granularity = self->row_granularity;
    ret->column_granularity = self->column_granularity;
    return ret;
}

void hve_grid_free(HVEGrid *self) {
    g_free(self);
}










HVELayout *hve_layout_copy(HVELayout *self) {
    HVELayout *ret = g_new0(HVELayout, 1);
    hve_layout_set_grid_map(ret, self->grid_map);
    return ret;
}

void hve_layout_free(HVELayout *self) {
    hve_layout_set_grid_map(self, NULL);
    g_free(self);
}










HVEMotionDetectionLayout *hve_motion_detection_layout_copy(HVEMotionDetectionLayout *self) {
    HVEMotionDetectionLayout *ret = g_new0(HVEMotionDetectionLayout, 1);
    ret->sensitivity_level = self->sensitivity_level;
    hve_motion_detection_layout_set_layout(ret, self->layout);
    return ret;
}

void hve_motion_detection_layout_free(HVEMotionDetectionLayout *self) {
    hve_motion_detection_layout_set_layout(self, NULL);
    g_free(self);
}










HVEMotionDetection *hve_motion_detection_copy(HVEMotionDetection *self) {
    HVEMotionDetection *ret = g_new0(HVEMotionDetection, 1);
    ret->enabled = self->enabled;
    hve_motion_detection_set_grid(ret, self->grid);
    hve_motion_detection_set_motion_detection_layout(ret, self->motion_detection_layout);
    return ret;
}

void hve_motion_detection_free(HVEMotionDetection *self) {
    hve_motion_detection_set_grid(self, NULL);
    hve_motion_detection_set_motion_detection_layout(self, NULL);
    g_free(self);
}










HVEEndpoint *hve_endpoint_copy(HVEEndpoint *self) {
    HVEEndpoint *ret = g_new0(HVEEndpoint, 1);
    hve_endpoint_set_name(ret, self->name);
    hve_endpoint_set_host(ret, self->host);
    hve_endpoint_set_ip(ret, self->ip);
    ret->port = self->port;
    hve_endpoint_set_path(ret, self->path);
    return ret;
}

void hve_endpoint_free(HVEEndpoint *self) {
    hve_endpoint_set_name(self, NULL);
    hve_endpoint_set_host(self, NULL);
    hve_endpoint_set_ip(self, NULL);
    hve_endpoint_set_path(self, NULL);
    g_free(self);
}
