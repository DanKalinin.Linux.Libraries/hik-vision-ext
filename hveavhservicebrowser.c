//
// Created by root on 30.09.2021.
//

#include "hveavhservicebrowser.h"

enum {
    HVE_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_FOUND = 1,
    HVE_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_REMOVED,
    _HVE_AVH_SERVICE_BROWSER_SIGNAL_COUNT
};

guint hve_avh_service_browser_signals[_HVE_AVH_SERVICE_BROWSER_SIGNAL_COUNT] = {0};

void hve_avh_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags);
void _hve_avh_service_browser_endpoint_found(HVEAVHServiceBrowser *self, HVEEndpoint *endpoint);
void _hve_avh_service_browser_endpoint_removed(HVEAVHServiceBrowser *self, gchar *name);
void hve_avh_service_browser_avh_s_service_resolver_event(AVHSServiceResolver *sender, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags, gpointer self);

G_DEFINE_TYPE(HVEAVHServiceBrowser, hve_avh_service_browser, AVH_TYPE_S_SERVICE_BROWSER)

static void hve_avh_service_browser_class_init(HVEAVHServiceBrowserClass *class) {
    AVH_S_SERVICE_BROWSER_CLASS(class)->event = hve_avh_service_browser_event;

    class->endpoint_found = _hve_avh_service_browser_endpoint_found;
    class->endpoint_removed = _hve_avh_service_browser_endpoint_removed;

    hve_avh_service_browser_signals[HVE_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_FOUND] = g_signal_new("endpoint-found", HVE_TYPE_AVH_SERVICE_BROWSER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(HVEAVHServiceBrowserClass, endpoint_found), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
    hve_avh_service_browser_signals[HVE_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_REMOVED] = g_signal_new("endpoint-removed", HVE_TYPE_AVH_SERVICE_BROWSER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(HVEAVHServiceBrowserClass, endpoint_removed), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
}

static void hve_avh_service_browser_init(HVEAVHServiceBrowser *self) {

}

void hve_avh_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags) {
    if (event == AVAHI_BROWSER_NEW) {
        HVEAVHServiceResolver *resolver = hve_avh_service_resolver_new(name, type, NULL);
        (void)g_signal_connect(resolver, "event", G_CALLBACK(hve_avh_service_browser_avh_s_service_resolver_event), self);
    } else if (event == AVAHI_BROWSER_REMOVE) {
        hve_avh_service_browser_endpoint_removed(HVE_AVH_SERVICE_BROWSER(self), name);
    }
}

void hve_avh_service_browser_endpoint_found(HVEAVHServiceBrowser *self, HVEEndpoint *endpoint) {
    g_signal_emit(self, hve_avh_service_browser_signals[HVE_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_FOUND], 0, endpoint);
}

void _hve_avh_service_browser_endpoint_found(HVEAVHServiceBrowser *self, HVEEndpoint *endpoint) {
    g_print("endpoint-found\n");
}

void hve_avh_service_browser_endpoint_removed(HVEAVHServiceBrowser *self, gchar *name) {
    g_signal_emit(self, hve_avh_service_browser_signals[HVE_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_REMOVED], 0, name);
}

void _hve_avh_service_browser_endpoint_removed(HVEAVHServiceBrowser *self, gchar *name) {
    g_print("endpoint-removed\n");
}

void hve_avh_service_browser_avh_s_service_resolver_event(AVHSServiceResolver *sender, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags, gpointer self) {
    if (event == AVAHI_RESOLVER_FOUND) {
        gchar ip[AVAHI_ADDRESS_STR_MAX] = {0};
        (void)avahi_address_snprint(ip, AVAHI_ADDRESS_STR_MAX, address);

        HVEEndpoint endpoint = {0};
        endpoint.name = name;
        endpoint.host = host;
        endpoint.ip = ip;
        endpoint.port = port;

        for (AvahiStringList *entry = txt; entry != NULL; entry = entry->next) {
            if (g_str_has_prefix((gchar *)entry->text, "path=")) {
                endpoint.path = (gchar *)entry->text + 5;
            }
        }

        hve_avh_service_browser_endpoint_found(self, &endpoint);
    }

    g_object_unref(sender);
}

HVEAVHServiceBrowser *hve_avh_service_browser_new(gchar *type, GError **error) {
    g_autoptr(HVEAVHServiceBrowser) self = g_object_new(HVE_TYPE_AVH_SERVICE_BROWSER, NULL);

    AVHServerDefault *server = NULL;
    if ((server = avh_server_default_shared(error)) == NULL) return NULL;

    g_autoptr(AvahiSServiceBrowser) object = NULL;
    if ((object = avh_s_service_browser_new(AVH_SERVER(server)->object, AVAHI_IF_UNSPEC, AVAHI_PROTO_INET, type, "local", 0, avh_s_service_browser_event_callback, self, error)) == NULL) return NULL;

    AVH_S_SERVICE_BROWSER(self)->object = g_steal_pointer(&object);

    return g_steal_pointer(&self);
}
