//
// Created by root on 09.09.2021.
//

#include "hvexmlebuilder.h"

xmlNodePtr hve_xmle_builder_challenge_get(HVEPublicKey *public_key) {
    xmlNodePtr self = hve_xmle_builder_public_key(public_key);
    return self;
}

xmlNodePtr hve_xmle_builder_activate(HVEActivateInfo *activate_info) {
    xmlNodePtr self = hve_xmle_builder_activate_info(activate_info);
    return self;
}

xmlNodePtr hve_xmle_builder_time_update(HVETime *time) {
    xmlNodePtr self = hve_xmle_builder_time(time);
    return self;
}

xmlNodePtr hve_xmle_builder_wireless_update(HVEWireless *wireless) {
    xmlNodePtr self = hve_xmle_builder_wireless(wireless);
    return self;
}

xmlNodePtr hve_xmle_builder_wireless_update_v1(HVEWireless *wireless) {
    xmlNodePtr self = hve_xmle_builder_wireless(wireless);
    return self;
}

xmlNodePtr hve_xmle_builder_ip_address_update(HVEIPAddress *ip_address) {
    xmlNodePtr self = hve_xmle_builder_ip_address(ip_address);
    return self;
}

xmlNodePtr hve_xmle_builder_streaming_channel_update(HVEStreamingChannel *streaming_channel) {
    xmlNodePtr self = hve_xmle_builder_streaming_channel(streaming_channel);
    return self;
}

xmlNodePtr hve_xmle_builder_motion_detection_update(HVEMotionDetection *motion_detection) {
    xmlNodePtr self = hve_xmle_builder_motion_detection(motion_detection);
    return self;
}

xmlNodePtr hve_xmle_builder_public_key(HVEPublicKey *public_key) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "PublicKey");
    (void)xmlNewChild(self, NULL, BAD_CAST "key", BAD_CAST public_key->key);
    return self;
}

xmlNodePtr hve_xmle_builder_activate_info(HVEActivateInfo *activate_info) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "ActivateInfo");
    (void)xmlNewChild(self, NULL, BAD_CAST "password", BAD_CAST activate_info->password);
    return self;
}

xmlNodePtr hve_xmle_builder_time(HVETime *time) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "Time");
    (void)xmlNewChild(self, NULL, BAD_CAST "timeMode", BAD_CAST time->time_mode);
    (void)xmlNewChild(self, NULL, BAD_CAST "localTime", BAD_CAST time->local_time);
    (void)xmlNewChild(self, NULL, BAD_CAST "timeZone", BAD_CAST time->time_zone);
    return self;
}

xmlNodePtr hve_xmle_builder_wpa(HVEWPA *wpa) {
    g_autofree gchar *wpa_key_length = g_strdup_printf("%i", wpa->wpa_key_length);

    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "WPA");
    (void)xmlNewChild(self, NULL, BAD_CAST "algorithmType", BAD_CAST wpa->algorithm_type);
    (void)xmlNewChild(self, NULL, BAD_CAST "sharedKey", BAD_CAST wpa->shared_key);
    (void)xmlNewChild(self, NULL, BAD_CAST "wpaKeyLength", BAD_CAST wpa_key_length);
    return self;
}

xmlNodePtr hve_xmle_builder_wireless_security(HVEWirelessSecurity *wireless_security) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "WirelessSecurity");
    (void)xmlNewChild(self, NULL, BAD_CAST "securityMode", BAD_CAST wireless_security->security_mode);

    if (wireless_security->wpa != NULL) {
        xmlNodePtr wpa = hve_xmle_builder_wpa(wireless_security->wpa);
        (void)xmlAddChild(self, wpa);
    }

    return self;
}

xmlNodePtr hve_xmle_builder_wireless(HVEWireless *wireless) {
    gchar *enabled = wireless->enabled ? "true" : "false";

    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "Wireless");
    (void)xmlNewChild(self, NULL, BAD_CAST "enabled", BAD_CAST enabled);
    (void)xmlNewChild(self, NULL, BAD_CAST "ssid", BAD_CAST wireless->ssid);

    if (wireless->wireless_security != NULL) {
        xmlNodePtr wireless_security = hve_xmle_builder_wireless_security(wireless->wireless_security);
        (void)xmlAddChild(self, wireless_security);
    }

    return self;
}

xmlNodePtr hve_xmle_builder_ipv6_mode(HVEIPv6Mode *ipv6_mode) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "Ipv6Mode");
    (void)xmlNewChild(self, NULL, BAD_CAST "ipV6AddressingType", BAD_CAST ipv6_mode->ipv6_addressing_type);
    return self;
}

xmlNodePtr hve_xmle_builder_ip_address(HVEIPAddress *ip_address) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "IPAddress");
    (void)xmlNewChild(self, NULL, BAD_CAST "ipVersion", BAD_CAST ip_address->ip_version);
    (void)xmlNewChild(self, NULL, BAD_CAST "addressingType", BAD_CAST ip_address->addressing_type);

    if (ip_address->ipv6_mode != NULL) {
        xmlNodePtr ipv6_mode = hve_xmle_builder_ipv6_mode(ip_address->ipv6_mode);
        (void)xmlAddChild(self, ipv6_mode);
    }

    return self;
}

xmlNodePtr hve_xmle_builder_video(HVEVideo *video) {
    g_autofree gchar *video_resolution_width = g_strdup_printf("%i", video->video_resolution_width);
    g_autofree gchar *video_resolution_height = g_strdup_printf("%i", video->video_resolution_height);
    g_autofree gchar *max_frame_rate = g_strdup_printf("%i", video->max_frame_rate);
    
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "Video");
    (void)xmlNewChild(self, NULL, BAD_CAST "videoCodecType", BAD_CAST video->video_codec_type);
    (void)xmlNewChild(self, NULL, BAD_CAST "videoResolutionWidth", BAD_CAST video_resolution_width);
    (void)xmlNewChild(self, NULL, BAD_CAST "videoResolutionHeight", BAD_CAST video_resolution_height);
    (void)xmlNewChild(self, NULL, BAD_CAST "videoQualityControlType", BAD_CAST video->video_quality_control_type);
    (void)xmlNewChild(self, NULL, BAD_CAST "maxFrameRate", BAD_CAST max_frame_rate);
    return self;
}

xmlNodePtr hve_xmle_builder_audio(HVEAudio *audio) {
    gchar *enabled = audio->enabled ? "true" : "false";

    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "Audio");
    (void)xmlNewChild(self, NULL, BAD_CAST "enabled", BAD_CAST enabled);
    (void)xmlNewChild(self, NULL, BAD_CAST "audioCompressionType", BAD_CAST audio->audio_compression_type);
    return self;
}

xmlNodePtr hve_xmle_builder_streaming_channel(HVEStreamingChannel *streaming_channel) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "StreamingChannel");
    
    if (streaming_channel->video != NULL) {
        xmlNodePtr video = hve_xmle_builder_video(streaming_channel->video);
        (void)xmlAddChild(self, video);
    }

    if (streaming_channel->audio != NULL) {
        xmlNodePtr audio = hve_xmle_builder_audio(streaming_channel->audio);
        (void)xmlAddChild(self, audio);
    }
    
    return self;
}

xmlNodePtr hve_xmle_builder_grid(HVEGrid *grid) {
    g_autofree gchar *row_granularity = g_strdup_printf("%i", grid->row_granularity);
    g_autofree gchar *column_granularity = g_strdup_printf("%i", grid->column_granularity);
    
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "Grid");
    (void)xmlNewChild(self, NULL, BAD_CAST "rowGranularity", BAD_CAST row_granularity);
    (void)xmlNewChild(self, NULL, BAD_CAST "columnGranularity", BAD_CAST column_granularity);
    return self;
}

xmlNodePtr hve_xmle_builder_layout(HVELayout *layout) {
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "layout");
    (void)xmlNewChild(self, NULL, BAD_CAST "gridMap", BAD_CAST layout->grid_map);
    return self;
}

xmlNodePtr hve_xmle_builder_motion_detection_layout(HVEMotionDetectionLayout *motion_detection_layout) {
    g_autofree gchar *sensitivity_level = g_strdup_printf("%i", motion_detection_layout->sensitivity_level);
    
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "MotionDetectionLayout");
    (void)xmlNewChild(self, NULL, BAD_CAST "sensitivityLevel", BAD_CAST sensitivity_level);
    
    if (motion_detection_layout->layout != NULL) {
        xmlNodePtr layout = hve_xmle_builder_layout(motion_detection_layout->layout);
        (void)xmlAddChild(self, layout);
    }
    
    return self;
}

xmlNodePtr hve_xmle_builder_motion_detection(HVEMotionDetection *motion_detection) {
    gchar *enabled = motion_detection->enabled ? "true" : "false";
    
    xmlNodePtr self = xmlNewNode(NULL, BAD_CAST "MotionDetection");
    (void)xmlNewChild(self, NULL, BAD_CAST "enabled", BAD_CAST enabled);
    
    if (motion_detection->grid != NULL) {
        xmlNodePtr grid = hve_xmle_builder_grid(motion_detection->grid);
        (void)xmlAddChild(self, grid);
    }
    
    if (motion_detection->motion_detection_layout != NULL) {
        xmlNodePtr motion_detection_layout = hve_xmle_builder_motion_detection_layout(motion_detection->motion_detection_layout);
        (void)xmlAddChild(self, motion_detection_layout);
    }
    
    return self;
}
