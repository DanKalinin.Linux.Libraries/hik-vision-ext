//
// Created by root on 07.09.2021.
//

#include "hvesoelogger.h"

G_DEFINE_TYPE(HVESOELogger, hve_soe_logger, SOE_TYPE_LOGGER)

static void hve_soe_logger_class_init(HVESOELoggerClass *class) {

}

static void hve_soe_logger_init(HVESOELogger *self) {

}

HVESOELogger *hve_soe_logger_new(void) {
    HVESOELogger *self = g_object_new(HVE_TYPE_SOE_LOGGER, "level", SOUP_LOGGER_LOG_BODY, "max-body-size", -1, NULL);
    return self;
}
