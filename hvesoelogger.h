//
// Created by root on 07.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVESOELOGGER_H
#define LIBRARY_HIK_VISION_EXT_HVESOELOGGER_H

#include "hvemain.h"

G_BEGIN_DECLS

#define HVE_TYPE_SOE_LOGGER hve_soe_logger_get_type()

GE_DECLARE_DERIVABLE_TYPE(HVESOELogger, hve_soe_logger, HVE, SOE_LOGGER, SOELogger)

struct _HVESOELoggerClass {
    SOELoggerClass super;
};

struct _HVESOELogger {
    SOELogger super;
};

HVESOELogger *hve_soe_logger_new(void);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVESOELOGGER_H
