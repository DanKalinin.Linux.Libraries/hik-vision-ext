//
// Created by root on 10.09.2021.
//

#include "hveerror.h"

G_DEFINE_QUARK(hve-error-quark, hve_error)

void hve_set_error(GError **self, HVEResponseStatus *response_status) {
    g_set_error_literal(self, HVE_ERROR, response_status->status_code, response_status->status_string);
}
