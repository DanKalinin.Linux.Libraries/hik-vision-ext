## hik-vision-ext

IP camera configuration SDK for Linux.

This library provides a binding interface for high-level languages and can be compiled for POSIX-compatible systems, such as Android and iOS.

- [iOS SDK](https://gitlab.com/DanKalinin.iOS.Frameworks/HikVisionExt.git)
- [Android SDK](https://gitlab.com/DanKalinin.Android.Projects/HikVisionExt.git)

![](images/sdk.jpeg)
