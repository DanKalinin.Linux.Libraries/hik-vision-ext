//
// Created by root on 07.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVESOEMESSAGE_H
#define LIBRARY_HIK_VISION_EXT_HVESOEMESSAGE_H

#include "hvemain.h"
#include "hveerror.h"
#include "hveschema.h"
#include "hvexmlebuilder.h"
#include "hvexmlereader.h"

G_BEGIN_DECLS

#define HVE_TYPE_SOE_MESSAGE hve_soe_message_get_type()

GE_DECLARE_DERIVABLE_TYPE(HVESOEMessage, hve_soe_message, HVE, SOE_MESSAGE, SOEMessage)

struct _HVESOEMessageClass {
    SOEMessageClass super;
};

struct _HVESOEMessage {
    SOEMessage super;
};

HVESOEMessage *hve_soe_message_activate_status_get(SoupURI *base);
HVEActivateStatus *hve_soe_message_activate_status_get_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_challenge_get(SoupURI *base, HVEPublicKey *public_key);
HVEChallenge *hve_soe_message_challenge_get_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_activate(SoupURI *base, HVEActivateInfo *activate_info);
HVEResponseStatus *hve_soe_message_activate_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_reboot(SoupURI *base);
HVEResponseStatus *hve_soe_message_reboot_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_device_info_get(SoupURI *base);
HVEDeviceInfo *hve_soe_message_device_info_get_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_time_update(SoupURI *base, HVETime *time);
HVEResponseStatus *hve_soe_message_time_update_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_network_interfaces_get(SoupURI *base);
gboolean hve_soe_message_network_interfaces_get_finish(HVESOEMessage *self, GList **network_interfaces, GError **error);

HVESOEMessage *hve_soe_message_wireless_get(SoupURI *base, gchar *id);
HVEWireless *hve_soe_message_wireless_get_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_wireless_update(SoupURI *base, gchar *id, HVEWireless *wireless);
HVEResponseStatus *hve_soe_message_wireless_update_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_wireless_update_v1(SoupURI *base, gchar *id, HVEWireless *wireless);
gboolean hve_soe_message_wireless_update_v1_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_ip_address_get(SoupURI *base, gchar *id);
HVEIPAddress *hve_soe_message_ip_address_get_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_ip_address_update(SoupURI *base, gchar *id, HVEIPAddress *ip_address);
HVEResponseStatus *hve_soe_message_ip_address_update_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_streaming_channel_update(SoupURI *base, gchar *id, HVEStreamingChannel *streaming_channel);
HVEResponseStatus *hve_soe_message_streaming_channel_update_finish(HVESOEMessage *self, GError **error);

HVESOEMessage *hve_soe_message_motion_detection_update(SoupURI *base, gchar *id, HVEMotionDetection *motion_detection);
HVEResponseStatus *hve_soe_message_motion_detection_update_finish(HVESOEMessage *self, GError **error);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVESOEMESSAGE_H
