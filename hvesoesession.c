//
// Created by root on 07.09.2021.
//

#include "hvesoesession.h"

void hve_soe_session_finalize(GObject *self);
void hve_soe_session_authenticate(SoupSession *self, SoupMessage *message, SoupAuth *auth, gboolean retrying);

G_DEFINE_TYPE(HVESOESession, hve_soe_session, SOE_TYPE_SESSION)

static void hve_soe_session_class_init(HVESOESessionClass *class) {
    G_OBJECT_CLASS(class)->finalize = hve_soe_session_finalize;

    SOUP_SESSION_CLASS(class)->authenticate = hve_soe_session_authenticate;
}

static void hve_soe_session_init(HVESOESession *self) {

}

void hve_soe_session_finalize(GObject *self) {
    hve_soe_session_set_base(HVE_SOE_SESSION(self), NULL);
    hve_soe_session_set_username(HVE_SOE_SESSION(self), NULL);
    hve_soe_session_set_password(HVE_SOE_SESSION(self), NULL);

    G_OBJECT_CLASS(hve_soe_session_parent_class)->finalize(self);
}

void hve_soe_session_authenticate(SoupSession *self, SoupMessage *message, SoupAuth *auth, gboolean retrying) {
    if (retrying) return;
    soup_auth_authenticate(auth, HVE_SOE_SESSION(self)->username, HVE_SOE_SESSION(self)->password);
}

HVESOESession *hve_soe_session_new(SoupURI *base, gchar *username, gchar *password) {
    HVESOESession *self = g_object_new(HVE_TYPE_SOE_SESSION, "accept-language-auto", TRUE, "timeout", 5, NULL);

    hve_soe_session_set_base(self, base);
    hve_soe_session_set_username(self, username);
    hve_soe_session_set_password(self, password);

    HVESOELogger *logger = hve_soe_logger_new();
    soup_session_add_feature(SOUP_SESSION(self), SOUP_SESSION_FEATURE(logger));

    return self;
}

HVEActivateStatus *hve_soe_session_activate_status_get_sync(HVESOESession *self, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_activate_status_get(self->base);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEActivateStatus *ret = hve_soe_message_activate_status_get_finish(message, error);
    return ret;
}

HVEChallenge *hve_soe_session_challenge_get_sync(HVESOESession *self, HVEPublicKey *public_key, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_challenge_get(self->base, public_key);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEChallenge *ret = hve_soe_message_challenge_get_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_activate_sync(HVESOESession *self, HVEActivateInfo *activate_info, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_activate(self->base, activate_info);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_activate_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_reboot_sync(HVESOESession *self, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_reboot(self->base);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_reboot_finish(message, error);
    return ret;
}

HVEDeviceInfo *hve_soe_session_device_info_get_sync(HVESOESession *self, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_device_info_get(self->base);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEDeviceInfo *ret = hve_soe_message_device_info_get_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_time_update_sync(HVESOESession *self, HVETime *time, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_time_update(self->base, time);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_time_update_finish(message, error);
    return ret;
}

gboolean hve_soe_session_network_interfaces_get_sync(HVESOESession *self, GList **network_interfaces, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_network_interfaces_get(self->base);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    gboolean ret = hve_soe_message_network_interfaces_get_finish(message, network_interfaces, error);
    return ret;
}

HVEWireless *hve_soe_session_wireless_get_sync(HVESOESession *self, gchar *id, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_wireless_get(self->base, id);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEWireless *ret = hve_soe_message_wireless_get_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_wireless_update_sync(HVESOESession *self, gchar *id, HVEWireless *wireless, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_wireless_update(self->base, id, wireless);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_wireless_update_finish(message, error);
    return ret;
}

gboolean hve_soe_session_wireless_update_v1_sync(HVESOESession *self, gchar *id, HVEWireless *wireless, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_wireless_update_v1(self->base, id, wireless);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    gboolean ret = hve_soe_message_wireless_update_v1_finish(message, error);
    return ret;
}

HVEIPAddress *hve_soe_session_ip_address_get_sync(HVESOESession *self, gchar *id, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_ip_address_get(self->base, id);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEIPAddress *ret = hve_soe_message_ip_address_get_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_ip_address_update_sync(HVESOESession *self, gchar *id, HVEIPAddress *ip_address, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_ip_address_update(self->base, id, ip_address);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_ip_address_update_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_streaming_channel_update_sync(HVESOESession *self, gchar *id, HVEStreamingChannel *streaming_channel, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_streaming_channel_update(self->base, id, streaming_channel);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_streaming_channel_update_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_motion_detection_update_sync(HVESOESession *self, gchar *id, HVEMotionDetection *motion_detection, GError **error) {
    g_autoptr(HVESOEMessage) message = hve_soe_message_motion_detection_update(self->base, id, motion_detection);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    HVEResponseStatus *ret = hve_soe_message_motion_detection_update_finish(message, error);
    return ret;
}

HVEResponseStatus *hve_soe_session_activate_v1_sync(HVESOESession *self, HVEActivateInfo *activate_info, GError **error) {
    g_auto(gnutls_x509_privkey_t) x509_privkey = NULL;
    if (tls_x509_privkey_init(&x509_privkey, error) < GNUTLS_E_SUCCESS) return NULL;
    if (tls_x509_privkey_import(x509_privkey, &hve_private_key, GNUTLS_X509_FMT_PEM, error) < GNUTLS_E_SUCCESS) return NULL;

    g_auto(gnutls_privkey_t) privkey = NULL;
    if (tls_privkey_init(&privkey, error) < GNUTLS_E_SUCCESS) return NULL;
    if (tls_privkey_import_x509(privkey, x509_privkey, 0, error) < GNUTLS_E_SUCCESS) return NULL;

    g_auto(gnutls_pubkey_t) pubkey = NULL;
    if (tls_pubkey_init(&pubkey, error) < GNUTLS_E_SUCCESS) return NULL;
    if (tls_pubkey_import_privkey(pubkey, privkey, 0, 0, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) m = g_new0(gnutls_datum_t, 1);
    g_autoptr(gnutls_datum_t) e = g_new0(gnutls_datum_t, 1);
    if (tls_pubkey_export_rsa_raw2(pubkey, m, e, GNUTLS_EXPORT_FLAG_NO_LZ, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) m_hex = g_new0(gnutls_datum_t, 1);
    if (tls_hex_encode2(m, m_hex, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) m_base64 = g_new0(gnutls_datum_t, 1);
    if (tls_base64_encode2(m_hex, m_base64, error) < GNUTLS_E_SUCCESS) return NULL;

    HVEPublicKey public_key = {0};
    public_key.key = (gchar *)m_base64->data;
    g_autoptr(HVEChallenge) challenge = NULL;
    if ((challenge = hve_soe_session_challenge_get_sync(self, &public_key, error)) == NULL) return NULL;

    gnutls_datum_t key_encrypted_base64 = {0};
    key_encrypted_base64.data = (guchar *)challenge->key;
    key_encrypted_base64.size = strlen(challenge->key);

    g_autoptr(gnutls_datum_t) key_encrypted_hex = g_new0(gnutls_datum_t, 1);
    if (tls_base64_decode2(&key_encrypted_base64, key_encrypted_hex, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) key_encrypted = g_new0(gnutls_datum_t, 1);
    if (tls_hex_decode2(key_encrypted_hex, key_encrypted, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) key_hex = g_new0(gnutls_datum_t, 1);
    if (tls_privkey_decrypt_data(privkey, 0, key_encrypted, key_hex, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) key = g_new0(gnutls_datum_t, 1);
    if (tls_hex_decode2(key_hex, key, error) < GNUTLS_E_SUCCESS) return NULL;

    guchar iv_data[16] = {0};
    gnutls_datum_t iv = {0};
    iv.data = iv_data;
    iv.size = 16;

    g_autofree gchar *right = g_strndup(activate_info->password, 16);

    guchar password_encrypted_data[32] = {0};

    g_auto(gnutls_cipher_hd_t) cipher_left = NULL;
    if (tls_cipher_init(&cipher_left, GNUTLS_CIPHER_AES_128_CBC, key, &iv, error) < GNUTLS_E_SUCCESS) return NULL;
    if (tls_cipher_encrypt2(cipher_left, key_hex->data, 16, password_encrypted_data, 16, error) < GNUTLS_E_SUCCESS) return NULL;

    g_auto(gnutls_cipher_hd_t) cipher_right = NULL;
    if (tls_cipher_init(&cipher_right, GNUTLS_CIPHER_AES_128_CBC, key, &iv, error) < GNUTLS_E_SUCCESS) return NULL;
    if (tls_cipher_encrypt2(cipher_right, right, 16, password_encrypted_data + 16, 16, error) < GNUTLS_E_SUCCESS) return NULL;

    gnutls_datum_t password_encrypted = {0};
    password_encrypted.data = password_encrypted_data;
    password_encrypted.size = 32;

    g_autoptr(gnutls_datum_t) password_encrypted_hex = g_new0(gnutls_datum_t, 1);
    if (tls_hex_encode2(&password_encrypted, password_encrypted_hex, error) < GNUTLS_E_SUCCESS) return NULL;

    g_autoptr(gnutls_datum_t) password_encrypted_base64 = g_new0(gnutls_datum_t, 1);
    if (tls_base64_encode2(password_encrypted_hex, password_encrypted_base64, error) < GNUTLS_E_SUCCESS) return NULL;

    HVEActivateInfo activate_info_encrypted_base64 = {0};
    activate_info_encrypted_base64.password = (gchar *)password_encrypted_base64->data;

    HVEResponseStatus *ret = NULL;
    if ((ret = hve_soe_session_activate_sync(self, &activate_info_encrypted_base64, error)) == NULL) return NULL;
    return ret;
}

HVEResponseStatus *hve_soe_session_time_update_v1_sync(HVESOESession *self, GError **error) {
    GDateTime *date_time = g_date_time_new_now_local();
    g_autofree gchar *local_time = g_date_time_format(date_time, "%FT%T");
    g_autofree gchar *time_zone = g_date_time_format(date_time, "CST%::z");
    time_zone[3] = (time_zone[3] == '+') ? '-' : '+';
    
    HVETime time = {0};
    time.time_mode = "manual";
    time.local_time = local_time;
    time.time_zone = time_zone;
    
    HVEResponseStatus *ret = NULL;
    if ((ret = hve_soe_session_time_update_sync(self, &time, error)) == NULL) return NULL;
    return ret;
}
