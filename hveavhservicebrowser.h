//
// Created by root on 30.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEAVHSERVICEBROWSER_H
#define LIBRARY_HIK_VISION_EXT_HVEAVHSERVICEBROWSER_H

#include "hvemain.h"
#include "hveschema.h"
#include "hveavhserviceresolver.h"

G_BEGIN_DECLS

#define HVE_TYPE_AVH_SERVICE_BROWSER hve_avh_service_browser_get_type()

GE_DECLARE_DERIVABLE_TYPE(HVEAVHServiceBrowser, hve_avh_service_browser, HVE, AVH_SERVICE_BROWSER, AVHSServiceBrowser)

struct _HVEAVHServiceBrowserClass {
    AVHSServiceBrowserClass super;

    void (*endpoint_found)(HVEAVHServiceBrowser *self, HVEEndpoint *endpoint);
    void (*endpoint_removed)(HVEAVHServiceBrowser *self, gchar *name);
};

struct _HVEAVHServiceBrowser {
    AVHSServiceBrowser super;
};

void hve_avh_service_browser_endpoint_found(HVEAVHServiceBrowser *self, HVEEndpoint *endpoint);
void hve_avh_service_browser_endpoint_removed(HVEAVHServiceBrowser *self, gchar *name);

HVEAVHServiceBrowser *hve_avh_service_browser_new(gchar *type, GError **error);

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEAVHSERVICEBROWSER_H
