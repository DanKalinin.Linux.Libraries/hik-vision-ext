//
// Created by root on 12.09.2021.
//

#ifndef LIBRARY_HIK_VISION_EXT_HVEPRIVATEKEY_H
#define LIBRARY_HIK_VISION_EXT_HVEPRIVATEKEY_H

#include "hvemain.h"

G_BEGIN_DECLS

extern gnutls_datum_t hve_private_key;

G_END_DECLS

#endif //LIBRARY_HIK_VISION_EXT_HVEPRIVATEKEY_H
